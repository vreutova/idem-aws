import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-kinesis-stream-" + str(int(time.time())),
    "shard_count": 10,
    "stream_mode_details": {"StreamMode": "PROVISIONED"},
    "retention_period_hours": 24,
    "encryption_type": "KMS",
    "shard_level_metrics": [
        "IncomingBytes",
        "IncomingRecords",
        "OutgoingBytes",
        "OutgoingRecords",
    ],
}
RESOURCE_TYPE = "aws.kinesis.stream"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    aws_kms_key,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["key_id"] = aws_kms_key.get("arn")

    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="exec-get-by-name", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    # This test  is here to avoid creating another Kinesis Stream fixture for exec.get() testing.
    ret = await hub.exec.aws.kinesis.stream.get(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update-invalid-shard-count", depends=["exec-get-by-name"])
async def test_update_invalid_shard_count(hub, ctx):
    PARAMETER["shard_count"] = 25
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert not response["result"], response["comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert (
            "InvalidArgumentException: An error occurred (InvalidArgumentException) when calling the UpdateShardCount "
            "operation: Cannot update shard count beyond 2x current shard count"
            in response["comment"]
        )
    else:
        assert (
            "InvalidArgumentException: An error occurred (InvalidArgumentException) when calling the UpdateShardCount "
            "operation: UpdateShardCount cannot scale up over double your current open shard count. Current open shard "
            "count: 10 Target shard count: 25" in response["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(
    name="update-shard-count", depends=["update-invalid-shard-count"]
)
async def test_update_shard_count(
    hub,
    ctx,
    __test,
):
    PARAMETER["shard_count"] = 20
    ctx["test"] = __test
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(
    name="update-invalid-retention-period", depends=["update-shard-count"]
)
async def test_update_invalid_retention_period(hub, ctx):
    PARAMETER["retention_period_hours"] = 18
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert not response["result"], response["comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert (
            "InvalidArgumentException: An error occurred (InvalidArgumentException) when calling the "
            "DecreaseStreamRetentionPeriod operation: Retention period hours 18 must be between 24 and 8760"
            in response["comment"]
        )
    else:
        assert (
            "InvalidArgumentException: An error occurred (InvalidArgumentException) when calling the "
            "DecreaseStreamRetentionPeriod operation: Minimum allowed retention period is 24 hours. "
            "Requested retention period (18 hours) is too short." in response["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(
    name="update-retention-period", depends=["update-invalid-retention-period"]
)
async def test_update_retention_period(
    hub,
    ctx,
    __test,
):
    PARAMETER["retention_period_hours"] = 36
    ctx["test"] = __test
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(
    name="update-shard-metrics", depends=["update-retention-period"]
)
async def test_update_shard_metrics(
    hub,
    ctx,
    __test,
):
    ctx["test"] = __test
    PARAMETER["shard_level_metrics"] = [
        "IncomingBytes",
        "IncomingRecords",
        "ReadProvisionedThroughputExceeded",
        "IteratorAgeMilliseconds",
    ]
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)

    PARAMETER["shard_level_metrics"] = [
        "ReadProvisionedThroughputExceeded",
        "IteratorAgeMilliseconds",
        "WriteProvisionedThroughputExceeded",
    ]
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update-tags", depends=["update-shard-metrics"])
async def test_update_tags(
    hub,
    ctx,
    __test,
):
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"], "Description": "A Kinesis Stream"}
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)

    PARAMETER["tags"] = {
        "Description": "A Kinesis Stream",
        "Summary": "Summary of Kinesis Stream",
    }
    response = await hub.states.aws.kinesis.stream.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["update-tags"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.kinesis.stream.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.kinesis.stream.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.kinesis.stream.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete AWS Kinesis Stream.
    ret = await hub.states.aws.kinesis.stream.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert_stream(resource=resource, parameters=PARAMETER, hub=hub, ctx=ctx)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.kinesis.stream.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.kinesis.stream.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
@pytest.mark.localstack(pro=False)
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.kinesis.stream.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_stream(resource, parameters, hub, ctx):
    assert parameters["name"] == resource.get("name")
    assert parameters["shard_count"] == resource.get("shard_count")
    assert len(parameters["shard_level_metrics"]) == len(
        resource.get("shard_level_metrics")
    )
    assert set(parameters["shard_level_metrics"]) == set(
        resource.get("shard_level_metrics")
    )
    assert parameters["retention_period_hours"] == resource.get(
        "retention_period_hours"
    )
    assert parameters["encryption_type"] == resource.get("encryption_type")
    assert parameters["key_id"] == resource.get("key_id")
    assert parameters["tags"] == resource.get("tags")
    # This field is not seen in 'DescribeStreamSummary(describe_stream_summary)' function
    # when test is executed on localstack
    if not hub.tool.utils.is_running_localstack(ctx):
        assert parameters["stream_mode_details"] == resource.get("stream_mode_details")
