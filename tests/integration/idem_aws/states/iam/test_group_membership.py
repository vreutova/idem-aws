import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_present(hub, ctx, aws_iam_user, aws_iam_group, __test):
    global PARAMETER
    ctx["test"] = __test
    group_name = aws_iam_group.get("group_name")
    PARAMETER["name"] = group_name
    PARAMETER["group"] = group_name
    PARAMETER["users"] = [aws_iam_user.get("user_name")]

    ret = await hub.states.aws.iam.group_membership.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.iam.group_membership '{PARAMETER['group']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.iam.group_membership '{PARAMETER['name']}'" in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_group_membership(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.iam.group_membership.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    resource_key = f"iam-group-membership-{resource_id}"
    assert resource_key in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.group_membership.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get(
        "aws.iam.group_membership.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_group_membership(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="modify_iam_group_membership_attributes", depends=["present"]
)
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_modify_attributes(hub, ctx, aws_iam_user_2, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["users"].append(aws_iam_user_2.get("user_name"))

    ret = await hub.states.aws.iam.group_membership.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    new_parameter["name"] = resource["name"]
    new_parameter["resource_id"] = resource["resource_id"]

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.iam.group_membership",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.iam.group_membership",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_group_membership(hub, ctx, ret["old_state"], PARAMETER)
    assert_group_membership(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_exec_get", depends=["modify_iam_group_membership_attributes"]
)
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.iam.group_membership.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_group_membership(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.group_membership.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        users=PARAMETER["users"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_group_membership(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.iam.group_membership",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.iam.group_membership",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
@pytest.mark.xfail(
    reason="AccessDenied: CreateUser operation is not authorized in AWS endpoint."
)
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.iam.group_membership.absent(
        ctx, name=PARAMETER["name"], users=PARAMETER["users"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.group_membership",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


def assert_group_membership(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("group") == resource.get("resource_id")
    assert parameters.get("group") == resource.get("group")
    assert sorted(parameters.get("users")) == sorted(resource.get("users"))
