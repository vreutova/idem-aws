"""Tests for validating EC2 VPC Endpoint Service Permissions."""
import uuid

import pytest


PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_present(
    hub, ctx, __test, aws_vpc_endpoint_service, aws_iam_user, aws_iam_user_2
):
    global PARAMETER
    ctx["test"] = __test

    assert (
        aws_vpc_endpoint_service
    ), "The vpc endpoint service might not have been created"
    assert aws_vpc_endpoint_service.get("resource_id")

    assert aws_iam_user, "The iam user might not have been created"
    assert aws_iam_user.get("arn")

    ret = await hub.states.aws.ec2.vpc_endpoint_service_permission.present(
        ctx,
        name=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user.get("arn"),
    )

    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}'"
            in ret["comment"]
        )

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
            ctx,
            name=PARAMETER["name"],
            service_id=aws_vpc_endpoint_service.get("resource_id"),
            principal_arn=aws_iam_user.get("arn"),
        )
        assert ret
        assert ret["result"]
        assert ret["ret"]
        resource = ret["ret"]
        assert resource
        assert PARAMETER["name"] == resource.get("name", None)
        assert resource.get("principal_arn")
        assert aws_iam_user.get("arn") == resource.get("principal_arn")

        # Now Update the resource - update to a new user
        ret = await hub.states.aws.ec2.vpc_endpoint_service_permission.present(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["name"],
            service_id=aws_vpc_endpoint_service.get("resource_id"),
            principal_arn=aws_iam_user_2.get("arn"),
        )
        assert (
            f"Updated aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}'"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]

        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert resource
        assert PARAMETER["name"] == resource.get("name", None)
        assert resource.get("principal_arn")
        assert aws_iam_user_2.get("arn") == resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_describe(hub, ctx, aws_iam_user_2):
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.states.aws.ec2.vpc_endpoint_service_permission.describe(
        ctx,
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        resource_id = PARAMETER["resource_id"]
        assert resource_id in ret
        assert "aws.ec2.vpc_endpoint_service_permission.present" in ret[resource_id]
        described_resource = ret[resource_id].get(
            "aws.ec2.vpc_endpoint_service_permission.present"
        )
        assert described_resource
        assert PARAMETER["name"] == described_resource.get("name", None)
        assert described_resource.get("principal_arn")
        assert aws_iam_user_2.get("arn") == described_resource.get("principal_arn")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_absent(hub, ctx, __test, aws_vpc_endpoint_service, aws_iam_user_2):
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test

    # Delete the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_permission.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["name"],
        service_id=aws_vpc_endpoint_service.get("resource_id"),
        principal_arn=aws_iam_user_2.get("arn"),
    )

    if __test:
        assert (
            f"Would delete aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}'"
            in ret["comment"]
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Now get the resource with exec - Should not exist
        # Now get the resource
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_permission.get(
            ctx,
            name=PARAMETER["name"],
            service_id=aws_vpc_endpoint_service.get("resource_id"),
            principal_arn=aws_iam_user_2.get("arn"),
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    if not __test:
        # Try deleting the resource again
        ret = await hub.states.aws.ec2.vpc_endpoint_service_permission.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["name"],
            service_id=aws_vpc_endpoint_service.get("resource_id"),
            principal_arn=aws_iam_user_2.get("arn"),
        )

        assert (
            f"aws.ec2.vpc_endpoint_service_permission '{PARAMETER['name']}' already absent"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]
        assert (not ret["old_state"]) and (not ret["new_state"])
        PARAMETER.pop("resource_id")
