import copy
import uuid

import pytest


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_policy_attachment(
    hub,
    ctx,
    aws_organization_unit,
    aws_organization_policy,
):
    attach_policy_organization = "idem-test-policy-organization-attachment-" + str(
        uuid.uuid4()
    )

    policy_id = aws_organization_policy["resource_id"]

    # attach policy to the target with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.organizations.policy_attachment.present(
        test_ctx,
        name=attach_policy_organization,
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would attach aws.organizations.policy_attachment {attach_policy_organization}"
        in str(ret["comment"])
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert attach_policy_organization == resource.get("name")
    assert policy_id == resource.get("policy_id")

    # attach policy to the target
    ret = await hub.states.aws.organizations.policy_attachment.present(
        ctx,
        name=attach_policy_organization,
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_id == resource.get("PolicyId")

    # describe policy attachments to organization root, account or OU
    describe_ret = await hub.states.aws.organizations.policy_attachment.describe(ctx)

    assert f"{aws_organization_unit['resource_id']}-{policy_id}" in describe_ret
    policy_attachment = describe_ret.get(
        f"{aws_organization_unit['resource_id']}-{policy_id}"
    ).get("aws.organizations.policy_attachment.present")
    assert policy_id == policy_attachment[0].get("policy_id")
    assert aws_organization_unit["resource_id"] == policy_attachment[1].get("target_id")

    # Detach the organization SCP policy from OU with test flag
    ret = await hub.states.aws.organizations.policy_attachment.absent(
        test_ctx,
        name="test",
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would detach aws.organizations.policy_attachment" in str(ret["comment"])

    # Detach the organization SCP policy from OU
    ret = await hub.states.aws.organizations.policy_attachment.absent(
        ctx,
        name="test",
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    ret = await hub.states.aws.organizations.policy_attachment.absent(
        ctx,
        name="test",
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
