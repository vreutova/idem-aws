#!/bin/bash

if command -v git
then
  git init || echo "Could not initialize git"
  git add .|| echo "Could not add files"
  if command -v pre-commit
  then
    pre-commit install || echo "pre-commit is not installed"
    SKIP="twine-check" pre-commit run -av || echo $?
    pre-commit autoupdate
  fi
  # Remove git/pre-commit file from the temporary directory
  rm -rf .git
  rm -rf .pre-commit-config.yaml
fi


if command -v tree
then
  tree .
fi
